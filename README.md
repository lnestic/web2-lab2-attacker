# Web-app for SQL Injection and CSRF attack Demonstration
### Advanced Web Development

Supporting **Next.js** web-app which initiates the CSRF attack on base web-app by sending a hidden GET request to logout route.

**It is not required to open this app anywhere**, the base app calls it during CSRF attack demonstration.

Deployed on **Vercel**:
```
https://web2-cute-cats.vercel.app/
```
